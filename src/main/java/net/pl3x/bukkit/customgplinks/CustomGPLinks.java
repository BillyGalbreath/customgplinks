package net.pl3x.bukkit.customgplinks;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import me.ryanhamshire.GriefPrevention.DataStore;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class CustomGPLinks extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        updateGPLinks();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.gplinks")) {
            sender.sendMessage(color("&4You do not have permission for that command!"));
            return true;
        }
        sender.sendMessage(color("&d" + getName() + " v" + getDescription().getVersion() + " reloaded."));
        reloadConfig();
        updateGPLinks();
        return true;
    }

    public void updateGPLinks() {
        setFinalStatic("SURVIVAL_VIDEO_URL", color(getConfig().getString("survival", "&3&nbit.ly/mcgpuser&r")));
        setFinalStatic("CREATIVE_VIDEO_URL", color(getConfig().getString("creative", "&3&nbit.ly/mcgpcrea&r")));
        setFinalStatic("SUBDIVISION_VIDEO_URL", color(getConfig().getString("subdivision", "&3&nbit.ly/mcgpsub&r")));
    }

    private void setFinalStatic(String name, String value) {
        try {
            Field field = DataStore.class.getDeclaredField(name);
            field.setAccessible(true);

            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

            field.set(null, value);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private String color(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }
}
